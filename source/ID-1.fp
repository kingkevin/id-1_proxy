Element["" "ISO17816-1 ID-1 card" "" "ID-1" 0 0 0 0 0 100 ""]
(
Pad[8.75mm 20.08mm 13.75mm 20.08mm 2.0mm 0 0 "" "1" ""]
Pad[8.75mm 22.62mm 13.75mm 22.62mm 2.0mm 0 0 "" "2" ""]
Pad[8.75mm 25.16mm 13.75mm 25.16mm 2.0mm 0 0 "" "3" ""]
Pad[8.75mm 27.7mm 13.75mm 27.7mm 2.0mm 0 0 "" "4" ""]
Pad[16.37mm 20.08mm 21.37mm 20.08mm 2.0mm 0 0 "" "5" ""]
Pad[16.37mm 22.62mm 21.37mm 22.62mm 2.0mm 0 0 "" "6" ""]
Pad[16.37mm 25.16mm 21.37mm 25.16mm 2.0mm 0 0 "" "7" ""]
Pad[16.37mm 27.7mm 21.37mm 27.7mm 2.0mm 0 0 "" "8" ""]
ElementLine[3.18mm 0mm 82.42mm 0mm 0.2mm]
ElementArc[82.42mm 3.18mm 3.18mm 3.18mm 180 90 0.2mm]
ElementLine[85.6mm 3.18mm 85.6mm 50.8mm 0.2mm]
ElementArc[82.42mm 50.8mm 3.18mm 3.18mm 90 90 0.2mm]
ElementLine[82.42mm 53.98mm 3.18mm 53.98mm 0.2mm]
ElementArc[3.18mm 50.8mm 3.18mm 3.18mm 0 90 0.2mm]
ElementLine[0mm 50.8mm 0mm 3.18mm 0.2mm]
ElementArc[3.18mm 3.18mm 3.18mm 3.18mm 270 90 0.2mm]
)
