# created by hand
# for ID-1 card slot FCI 52400
# datasheet http://portal.fciconnect.com/Comergent/fci/drawing/52400.pdf
Element["" "ID-1 card slot" "" "FCI-52400" 0 0 0 0 0 100 ""]
(
	# package
	ElementLine[0mm 0mm 62mm 0mm 0.2mm]
	ElementLine[62mm 0mm 62mm 40mm 0.2mm]
	ElementLine[62mm 40mm 0mm 40mm 0.2mm]
	ElementLine[0mm 40mm 0mm 0mm 0.2mm]
	# mounting holes
	Pin[2mm 10mm 3.2mm 0mm 0mm 3.2mm "" "" "hole"]
	Pin[60mm 10mm 3.2mm 0mm 0mm 3.2mm "" "" "hole"]
	Pin[2mm 30mm 3.2mm 0mm 0mm 3.2mm "" "" "hole"]
	Pin[60mm 30mm 3.2mm 0mm 0mm 3.2mm "" "" "hole"]
	# indent holes
	Pin[2mm 20mm 2.2mm 0mm 0mm 2.2mm "" "" "hole"]
	Pin[60mm 20mm 2.2mm 0mm 0mm 2.2mm "" "" "hole"]
	# smart card contacs
	Pin[37.88mm 39.05mm 2mm 0mm 0mm 1mm "C1" "1" ""]
	Pin[35.34mm 39.05mm 2mm 0mm 0mm 1mm "C2" "2" ""]
	Pin[32.80mm 39.05mm 2mm 0mm 0mm 1mm "C3" "3" ""]
	Pin[30.26mm 39.05mm 2mm 0mm 0mm 1mm "C4" "4" ""]
	Pin[37.88mm 0.95mm 2mm 0mm 0mm 1mm "C5" "5" ""]
	Pin[35.34mm 0.95mm 2mm 0mm 0mm 1mm "C6" "6" ""]
	Pin[32.80mm 0.95mm 2mm 0mm 0mm 1mm "C7" "7" ""]
	Pin[30.26mm 0.95mm 2mm 0mm 0mm 1mm "C8" "8" ""]
	# switch contact
	Pin[26.45mm 39.05mm 2mm 0mm 0mm 1mm "SW1" "9" ""]
	Pin[26.45mm 36.51mm 2mm 0mm 0mm 1mm "SW2" "10" ""]
)
